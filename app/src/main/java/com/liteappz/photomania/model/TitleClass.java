package com.liteappz.photomania.model;

public class TitleClass {
    public String ImageURL;
    public String Title;
    public String NumPhotos;

    public TitleClass (String title, String numPhotos, String imageURL) {
        this.Title = title;
        this.NumPhotos = numPhotos;
        this.ImageURL = imageURL;
    }

    public void setImageURL(String imageURL) {
        ImageURL = imageURL;
    }

    public void setNumPhotos(String numPhotos) {
        NumPhotos = numPhotos;
    }

    public void setTitle(String title) {
        Title = title;
    }


    public String getImageURL() {
        return ImageURL;
    }

    public String getNumPhotos() {
        return NumPhotos;
    }

    public String getTitle() {
        return Title;
    }
}
