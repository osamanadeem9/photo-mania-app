package com.liteappz.photomania.adapter;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.card.MaterialCardView;
import com.liteappz.photomania.R;
import com.liteappz.photomania.UI.PhotosFragment;
import com.liteappz.photomania.model.TitleClass;
import com.squareup.picasso.Picasso;

import java.util.List;

public class RecyclerViewAdapter_Photos extends RecyclerView.Adapter<RecyclerViewAdapter_Photos.MyViewHolder> {

    private Context mContext ;
    private List<String> mData ;


    public RecyclerViewAdapter_Photos(Context mContext, List<String> mData) {
        this.mContext = mContext;
        this.mData = mData;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view ;
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        view = mInflater.inflate(R.layout.cardview_photo, parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        Picasso.get().load(mData.get(position)).into(holder.photoImageView);

//        holder.cardView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                PhotosFragment ldf = new PhotosFragment();
//                Bundle args = new Bundle();
//                args.putString("title", mData.get(position).getTitle());
//
//                ldf.setArguments(args);
//
//                ((AppCompatActivity)mContext).getSupportFragmentManager()
//                        .beginTransaction()
//                        .replace(R.id.frame_container, ldf)
//                        .addToBackStack(null)
//                        .commit();
//
////                Intent intent = new Intent(mContext, BookDetailsActivity.class);
////                mContext.startActivity(intent);
//            }
//        });



    }

    @Override
    public int getItemCount() {
        return mData.size();
    }


    public static class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView photoImageView;
        MaterialCardView cardView ;

        public MyViewHolder(View itemView) {
            super(itemView);

            photoImageView =  itemView.findViewById(R.id.image_CardViewPhoto);
            cardView = itemView.findViewById(R.id.cardview_CardViewPhoto);
        }
    }
}