package com.liteappz.photomania.adapter;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.card.MaterialCardView;
import com.liteappz.photomania.R;
import com.liteappz.photomania.UI.PhotosFragment;
import com.liteappz.photomania.model.TitleClass;
import com.squareup.picasso.Picasso;

import java.util.List;

public class RecyclerViewAdapter_Title extends RecyclerView.Adapter<RecyclerViewAdapter_Title.MyViewHolder> {

    private Context mContext ;
    private List<TitleClass> mData ;
    private double subtotal = 0.0;

    private boolean isChecked = true;

    public RecyclerViewAdapter_Title(Context mContext, List<TitleClass> mData) {
        this.mContext = mContext;
        this.mData = mData;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view ;
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        view = mInflater.inflate(R.layout.cardview_title, parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        holder.titleTextView.setText(mData.get(position).getTitle());
        holder.numPhotos.setText(mData.get(position).getNumPhotos());

        Picasso.get().load(mData.get(position).getImageURL()).into(holder.coverImageView);

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                PhotosFragment ldf = new PhotosFragment();
                Bundle args = new Bundle();
                args.putString("title", mData.get(position).getTitle());

                ldf.setArguments(args);

                ((AppCompatActivity)mContext).getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.frame_container, ldf)
                        .addToBackStack(null)
                        .commit();

//                Intent intent = new Intent(mContext, BookDetailsActivity.class);
//                mContext.startActivity(intent);
            }
        });



    }

    @Override
    public int getItemCount() {
        return mData.size();
    }


    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView titleTextView, numPhotos;
        ImageView coverImageView;
        MaterialCardView cardView ;

        public MyViewHolder(View itemView) {
            super(itemView);

            titleTextView =  itemView.findViewById(R.id.title_CardViewTitle) ;
            numPhotos =  itemView.findViewById(R.id.numPhotos_CardViewTitle);
            coverImageView =  itemView.findViewById(R.id.coverImage_CardViewTitle);
            cardView = itemView.findViewById(R.id.cardview_CardViewTitle);
        }
    }
}