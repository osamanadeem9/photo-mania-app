package com.liteappz.photomania.UI;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.liteappz.photomania.R;
import com.liteappz.photomania.adapter.RecyclerViewAdapter_Photos;
import com.liteappz.photomania.adapter.RecyclerViewAdapter_Title;
import com.liteappz.photomania.model.TitleClass;
import com.liteappz.photomania.networking.Requests;
import com.liteappz.photomania.networking.URLs;
import com.veinhorn.scrollgalleryview.ScrollGalleryView;
import com.veinhorn.scrollgalleryview.builder.GallerySettings;

import java.util.ArrayList;

import static com.veinhorn.scrollgalleryview.loader.picasso.dsl.DSL.image;

public class PhotosFragment extends Fragment {

    ArrayList photos_list;
    RecyclerView recyclerViewPhotos;
    TextView textTopBar;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_photos, null);

        String titlePhotos = getArguments().getString("title");


        recyclerViewPhotos = v.findViewById(R.id.recyclerviewPhotos_PhotosFrag);
        textTopBar = v.findViewById(R.id.textTopBar_PhotosFrag);

        textTopBar.setText(titlePhotos);

        photos_list = new ArrayList<String>();
        RecyclerViewAdapter_Photos rvAdapterPhotos = new RecyclerViewAdapter_Photos(getActivity(), photos_list);
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());

        Requests.getPhotosFromPixabay(URLs.PHOTOS_LIST_URL+titlePhotos,
                photos_list, rvAdapterPhotos, recyclerViewPhotos, getActivity(), requestQueue);

        return v;
    }
}