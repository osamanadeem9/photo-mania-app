package com.liteappz.photomania.UI;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.liteappz.photomania.R;
import com.liteappz.photomania.adapter.RecyclerViewAdapter_Title;
import com.liteappz.photomania.model.TitleClass;
import com.liteappz.photomania.networking.Requests;
import com.liteappz.photomania.networking.URLs;

import java.util.ArrayList;

public class TopicsFragment extends Fragment {

    EditText searchEditText;
    ImageView searchButton;
    RecyclerView recyclerViewTopics;
    ArrayList titlesList;
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_topics, null);

        initializeVals(v);

        titlesList = new ArrayList<TitleClass>();
        RecyclerViewAdapter_Title rvAdapterTopics = new RecyclerViewAdapter_Title(getActivity(), titlesList);
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        Requests.getTitlesFromUnsplash(URLs.TITLES_LIST_URL, titlesList, rvAdapterTopics, recyclerViewTopics, getActivity(), requestQueue);

        return v;
    }

    private void initializeVals(View v) {
        searchEditText = v.findViewById(R.id.searchBarEditText_TopicsFrag);
        searchButton = v.findViewById(R.id.searchButton_TopicsFrag);
        recyclerViewTopics = v.findViewById(R.id.recyclerviewTopics_TopicsFrag);
    }


}