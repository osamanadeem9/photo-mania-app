package com.liteappz.photomania.networking;

import android.app.ProgressDialog;
import android.content.Context;
import android.widget.Toast;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.liteappz.photomania.model.TitleClass;
import com.veinhorn.scrollgalleryview.ScrollGalleryView;
import com.veinhorn.scrollgalleryview.builder.GallerySettings;

import java.util.ArrayList;

public class Requests {

    public static void getTitlesFromUnsplash(String url, ArrayList titles_list, RecyclerView.Adapter myAdapter, RecyclerView myrv, Context context, RequestQueue requestQueue){
        ProgressDialog pd = new ProgressDialog(context);
        pd.setMessage("Loading...");
        pd.show();

        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                JsonArray responseArray = JsonParser.parseString(response).getAsJsonArray();
                if (!responseArray.isJsonNull()){
                    for (Object titlesObj: responseArray){
                        JsonObject titleObj = (JsonObject)titlesObj;

                        JsonObject coverPhotoObj = titleObj.get("cover_photo").getAsJsonObject();
                        JsonObject coverPhotoURLsObj = coverPhotoObj.get("urls").getAsJsonObject();

                        String titleString = titleObj.get("title").getAsString();
                        int numPhotos = titleObj.get("total_photos").getAsInt();
                        String coverPhotoThumbURL = coverPhotoURLsObj.get("thumb").getAsString();

                        TitleClass titleClass = new TitleClass(
                                titleString,
                                numPhotos+" Photos",
                                coverPhotoThumbURL
                        );
                        titles_list.add(titleClass);
                    }
//                    if (titles_list.size()>0){
//                        noItemsTextview.setVisibility(View.GONE);
//                    }else {
//                        noItemsTextview.setVisibility(View.VISIBLE);
//                    }
                }
//                myrv.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL,false));
                myrv.setLayoutManager(new GridLayoutManager(context, 2));
                myrv.setAdapter(myAdapter);

                pd.dismiss();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                pd.dismiss();
                Toast.makeText(context, error.toString(), Toast.LENGTH_SHORT).show();
            }
        });
        requestQueue.add(request);
    }


    public static void getPhotosFromPixabay(String url, ArrayList photo_urls_list, RecyclerView.Adapter myAdapter, RecyclerView myrv, Context context, RequestQueue requestQueue){
        ProgressDialog pd = new ProgressDialog(context);
        pd.setMessage("Loading...");
        pd.show();

        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                JsonObject responseObj = JsonParser.parseString(response).getAsJsonObject();
                JsonArray hitsArray = responseObj.get("hits").getAsJsonArray();
                if (!hitsArray.isJsonNull()){
                    for (Object hitObj: hitsArray){
                        JsonObject hitJsonObj = (JsonObject)hitObj;

//                        String previewImageURL = hitJsonObj.get("previewURL").getAsString();
//                        String largeImageURL = hitJsonObj.get("largeImageURL").getAsString();
                        String webFormatURL = hitJsonObj.get("webformatURL").getAsString();

                        photo_urls_list.add(webFormatURL);
                    }
//                    if (titles_list.size()>0){
//                        noItemsTextview.setVisibility(View.GONE);
//                    }else {
//                        noItemsTextview.setVisibility(View.VISIBLE);
//                    }
                }
//                myrv.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL,false));

                myrv.setLayoutManager(new GridLayoutManager(context, 2));
                myrv.setAdapter(myAdapter);
                pd.dismiss();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                pd.dismiss();
                Toast.makeText(context, error.toString(), Toast.LENGTH_SHORT).show();
            }
        });
        requestQueue.add(request);
    }
}
